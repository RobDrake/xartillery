var express = require('express'),
	logger = require('morgan'),
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	methodOverride = require('method-override'),
	session = require('express-session'),
	passport = require('passport'),
	LocalStrategy = require('passport-local'),
	GoogleStrategy = require('passport-google'),
	FacebookStrategy = require('passport-facebook');

var path = require('path');
var routes = require('./routes/index');

var app = express();

//Passport configuration


//Express Configuration
app.set('views', __dirname + '/views');
app.set('view-engine', 'ejs');
app.use(logger('combined'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(session({secret: 'robdrake-xartillery-foobar', saveUninitialited: true, resave: true}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static( __dirname + '/public'));
app.use('/', express.static( __dirname + '/public/html'));




//port
app.listen(8080);
console.log('Listening on localhost:8080');
