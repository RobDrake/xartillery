xartillery is a server authoritative, turn based artillery game

4 Things need to be done:
	1. Authentication
	2. Game lobby
	3. Game server
	4. Game client

1. Authentication
	Done with passport, via local, google, and facebook
2. Game lobby
	After Player is authenticated, joins Game lobby
	Players can spectate on a game, create a game, or join a game
3. Game server
	Real 'meat' of the program
	Handles Authentication, Serving the game lobby, and bounds checking / kills
		of running games.
	Express app, mostly server from public/, no views needed or wanted
4. Game client
	Talks to Game Server, renders what Game Server tells it to
	uses Phaser.js
